# frameworktest

[![CI Status](http://img.shields.io/travis/HunterXu/frameworktest.svg?style=flat)](https://travis-ci.org/HunterXu/frameworktest)
[![Version](https://img.shields.io/cocoapods/v/frameworktest.svg?style=flat)](http://cocoapods.org/pods/frameworktest)
[![License](https://img.shields.io/cocoapods/l/frameworktest.svg?style=flat)](http://cocoapods.org/pods/frameworktest)
[![Platform](https://img.shields.io/cocoapods/p/frameworktest.svg?style=flat)](http://cocoapods.org/pods/frameworktest)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

frameworktest is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "frameworktest"
```

## Author

HunterXu, zhanwei.xu@mfashion.com.cn

## License

frameworktest is available under the MIT license. See the LICENSE file for more info.
