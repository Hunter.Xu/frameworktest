//
//  OFAppDelegate.h
//  frameworktest
//
//  Created by HunterXu on 10/29/2015.
//  Copyright (c) 2015 HunterXu. All rights reserved.
//

@import UIKit;

@interface OFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
