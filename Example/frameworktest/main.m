//
//  main.m
//  frameworktest
//
//  Created by HunterXu on 10/29/2015.
//  Copyright (c) 2015 HunterXu. All rights reserved.
//

@import UIKit;
#import "OFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OFAppDelegate class]));
    }
}
